.. uStore documentation master file, created by
   sphinx-quickstart on Fri Jul  5 11:54:21 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=================================
uStore doc - 0.0.1
=================================

.. image:: https://img.shields.io/readthedocs/ustore/latest?style=flat  :alt: Documentation
.. image:: https://img.shields.io/matrix/uprefer_ustore:converser.eu?label=Matrix&style=flat   :alt: Matrix
.. image:: https://coveralls.io/repos/gitlab/uprefer/ustore/ustore-api/badge.svg
.. image:: https://gitlab.com/uprefer/ustore/ustore-api/badges/master/pipeline.svg
Bienvenue sur la documentation officielle de uStore.

uStore est un **magasin d'applications mobile auto-hébergeable** et déployable facilement dans le cloud.

Il fonctionne avec les applications à destination des systèmes d'exploitations Android et iOS.

Mais surtout, il est **open source** !

----------
Que faire avec uStore ?
----------

uStore est exploitable dans plusieurs cas:

- Déployer les applications en developpement dans le process d'intégration (CI).
- Déployer des versions beta (CD).
- Déployer des versions release (CD).
- Déployer des variantes d'une même version.
- Conserver l'historique des versions, sans écraser l'application à chaque nouvelle publication.
- Déployer une instance de uStore dans un SI fermé afin de ne proposer les applications qu'aux membres de ce SI.

Il reste encore plein de cas d'usages à inventer, mais pour celà nous vous faisons confiance !

--------
Contraintes par OS
--------

Si la solution en elle même est gratuite, il existe des contraintes pour l'utiliser.

Pour **Android**, Google fait bien les choses. Il n'est pas nécessaire d'avoir une licence de developpeur pour publier vos applications dans uStore.

En revanche, pour les applications **iOS**, Apple a mis des barières.
Pour publier une application iOS via uStore, il est nécessaire de posséder l'une des deux **licences** suivantes:

- Une licence **Developper** avec possibilité de publier vos application via un certificat **AddHoc**
- Une licence **Apple Enterprise** avec un certificat **In-House**.

Il faudra signer vos applications à l'aide de ces comptes et des **certificats** associés.


-----
uStore: une solution multi applications
-----

uStore est avant tout un ecosystème composé de plusieurs applications travaillant ensemble.

Dans uStore, on trouve:

- uStore API: l'API au centre de tout.
- uStore App: le client mobile qui va bien.
- uStore Web: le front web pour se passer de l'application mobile.
- uStore CLI: une cli (command line interface) pour vous aider dans vos plateformes de CI/CD.

.. toctree::
   :maxdepth: 5
   :caption: uStore

   api
   cli
   mobile

.. toctree::
   :maxdepth: 2
   :caption: A propos

   about/uprefer