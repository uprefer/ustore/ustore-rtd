############
Deploy [WIP]
############

You want to deploy your own uStore instance ?

Congratulations, your will enter in the uStore World !

====
How ?
====
We just provide one way to deploy uStore: Docker.

Why ? For many reasons:

- It's simple
- It's better
- We are lazy developpers

======
What I need ?
======

To install Pocus API, you need some tools installed on your server:

- Docker (latest version)
- Docker-compose

We recommand to use any reverse proxy (`Traefik` is you friend)
.. _a Traefik: https://traefik.io/


=====
Install from DockerHub image
=====

[WIP]

======
From sources
======



------
Configuration
------

uStore API configuration is set in .env files: adamnapi.env && .env



Default uStoreapi.env: ::  

    # uStore API Global
    uStore_MODE=DEV
    uStore_MAP_PORT=8081
    uStore_PORT=80
    
    # Database Configuration
    DB_TYPE=mysql
    DB_HOST=db
    DB_PORT=3306
    DB_NAME=uStore
    DB_USER=uStoreAPI
    DB_PASSWORD=SFGDBVHCDst
    
    #Storage Service
    SSERVICE_HOST=storage_service
    SSERVICE_PORT=80
    SSERVICE_EXT_HOST=localhost
    SSERVICE_EXT_PORT=8080
    SSERVICE_MAP_PORT=8080

Default .env: ::
    
    # uStoreAPI
    uStore_MAP_PORT=8081
    
    # MySQL
    MYSQL_USER=uStoreAPI
    MYSQL_PASSWORD=SFGDBVHCDst
    MYSQL_ROOT_PASSWORD=smcngshlckbcflsx
    MYSQL_DATABASE=uStore
    
    # StorageService
    SSERVICE_MAP_PORT=8080
    GIN_MODE=debug
    STORAGESERVICE_MONGO_IP=mongo
    STORAGESERVICE_MONGO_PORT=27017
    STORAGESERVICE_MONGO_USER=StorageService
    STORAGESERVICE_MONGO_PASSWORD=StorageService
    STORAGESERVICE_MONGO_DB=StorageService
    STORAGESERVICE_PORT=80
    
    # Mongo
    MONGO_INITDB_ROOT_USERNAME=StorageService
    MONGO_INITDB_ROOT_PASSWORD=StorageService
    MONGO_INITDB_DATABASE=StorageService

