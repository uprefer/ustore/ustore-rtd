############
Api 
############

L'API de uStore est le centre de la solution.

Cette API gère l'ensemble des fonctionnalités liées à la publication et la gestion des applications.

-----
Deploiement
-----

La solution la plus simple et la plus rapide pour déployer une instance de uStore est l'utilisation de Docker.

_________
Dépendances
_________

L'API nécéssite la mise en place de services tiers pour fonctionner:

- MySQL_
- StorageService_ : un service de stockage écrit en go utlisant mongodb

Nous conseillons de déployer ces différents services via docker, pour plus de simplicité vous pouvez réaliser ce déploiement à partir du docker-compose de uStore API.

_________
Via docker compose
_________

Afin de déployer la stack complète pour l'API, vous pouvez utiliser le docker-compose suivant:

.. code-block:: yaml
    :linenos:

    

_________
A la main
_________

-----
Pour les developpeurs 
-----

_________
Documentation API
_________


.. _MySQL: https://www.mysql.com/
.. _StorageService: https://gitlab.com/uprefer/storageservice